using System;
using Matchmaking;
using Matchmaking.Requestors;
using Matchmaking.Models;
using Matchmaking.RoomIdGetters;
using Matchmaking.ServiceFlows;
using MetagameRequestor;
using MetagameRequestor.Serializer;
using UnityEngine;

public class TestMatchmakingRequests : MonoBehaviour, ICoroutineHelper
{
    private string userId;
    private string roomId;
    private RoomService roomService;
    private MatchmakingService matchmakingService;

    private void Awake()
    {
        userId = Guid.NewGuid().ToString("D");

        var serializer = new SimpleJsonSerializer();
        var httpRequestor = RequestorFactory.NewHttpRequestor(this);
        var roomRequestor = new RoomHttpRequestor(httpRequestor);
        var matchmakingRequestor = new MatchmakingHttpRequestor(httpRequestor);
        var matchmakingConfig = new MatchmakingConfig
        {
            joinMatchTimeoutSeconds = 10,
            waitRoomReadyTimeoutSeconds = 20000,
        };

        roomService = new RoomService(serializer, new RoomIdGetterFromHttpRequest(8082), roomRequestor);
        matchmakingService = new MatchmakingService(matchmakingConfig, this, matchmakingRequestor);
        
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            JoinMatch();
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            StopJoinMatch();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            InitializeRoomService();
        }
    }

    private void JoinMatch()
    {
        matchmakingService.JoinMatch(userId,
            onSuccess: roomData =>
            {
                // TODO: Go to gameplay scene
            },
            onError: error =>
            {
            });
    }

    private void StopJoinMatch()
    {
        matchmakingService.StopJoinMatch(userId);
    }

    private void InitializeRoomService()
    {
        roomService.Ready += () =>
        {
            var roomData = roomService.GetRoomData<MatchConfig, PlayerInfo>();
        };
        
        roomService.Initialize();
    }

    [Serializable]
    public class MatchConfig
    {
        public int maxNumberOfPlayers;
        public int matchDurationSeconds;
    }
    
    [Serializable]
    public class PlayerInfo
    {
        public string userId;
    }
}
