using System;
using Matchmaking.Requestors;
using Matchmaking.Models;
using Matchmaking.ServiceFlows;
using MetagameRequestor;
using MetagameRequestor.Models;
using UnityEngine;

namespace Matchmaking
{
    public class MatchmakingService
    {
        private enum State
        {
            None,
            JoinMatch,
        }

        private readonly IMatchmakingRequestor matchmakingRequestor;

        private readonly IsRoomReadyFlow isRoomReadyFlow;
        private readonly HasFoundMatchFlow hasFoundMatchFlow;

        private State currentState;

        public MatchmakingService(MatchmakingConfig config,
            ICoroutineHelper coroutineHelper,
            IMatchmakingRequestor matchmakingRequestor)
        {
            this.matchmakingRequestor = matchmakingRequestor;

            isRoomReadyFlow = new IsRoomReadyFlow(config, coroutineHelper, matchmakingRequestor);
            hasFoundMatchFlow = new HasFoundMatchFlow(config, coroutineHelper, matchmakingRequestor);

            currentState = State.None;
        }

        public void JoinMatch(string userId, Action<ClientRoomData> onSuccess, Action<Error> onError)
        {
            if (currentState != State.None) { return; }

            currentState = State.JoinMatch;
            
            matchmakingRequestor.JoinMatch(userId,
                onSuccess: () =>
                {
                    Debug.Log($"JoinMatch | Success");
                    
                    hasFoundMatchFlow.Run(userId,
                        onSuccess: hasFoundMatchResponse =>
                        {
                            isRoomReadyFlow.Run(hasFoundMatchResponse.roomId,
                                onSuccess: isRoomReadyResponse =>
                                {
                                    onSuccess?.Invoke(isRoomReadyResponse.roomData);
                                },
                                onError: error =>
                                {
                                    currentState = State.None;
                                    onError?.Invoke(error);
                                });
                        },
                        onError: error =>
                        {
                            currentState = State.None;
                            onError?.Invoke(error);
                        });
                },
                onError: error =>
                {
                    Debug.Log($"JoinMatch | Error | [code: {error.Code}] [message: {error.Message}]");

                    currentState = State.None;
                    onError?.Invoke(error);
                });
        }

        public void StopJoinMatch(string userId)
        {
            if (currentState != State.JoinMatch) { return; }

            currentState = State.None;
            
            hasFoundMatchFlow.Stop();

            matchmakingRequestor.StopJoinMatch(userId,
                onSuccess: () =>
                {
                    Debug.Log($"StopJoinMatch | Success");
                },
                onError: error =>
                {
                    Debug.Log($"StopJoinMatch | Error | [code: {error.Code}] [message: {error.Message}]");
                });
        }
    }
}