namespace Matchmaking.Constants
{
    public class Errors
    {
        public const string TimeoutErrorCode = "Matchmaking-Error-001";
        public const string TimeoutErrorMessage = "Timeout";
    }
}