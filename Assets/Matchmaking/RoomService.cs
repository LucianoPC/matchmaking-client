using System;
using Matchmaking.Models;
using Matchmaking.Requestors;
using Matchmaking.RoomIdGetters;
using MetagameRequestor.Serializer;
using UnityEngine;

namespace Matchmaking.ServiceFlows
{
    public class RoomService
    {
        private readonly ISerializer serializer;
        private readonly IRoomIdGetter roomIdGetter;
        private readonly IRoomRequestor roomRequestor;
        
        private bool initialized;
        private ServerRoomDataPayload roomDataPayload;

        public event Action Ready;

        public RoomService(ISerializer serializer, IRoomIdGetter roomIdGetter, IRoomRequestor roomRequestor)
        {
            this.serializer = serializer;
            this.roomIdGetter = roomIdGetter;
            this.roomRequestor = roomRequestor;
        }
        
        public void Initialize()
        {
            if (initialized) { return; }

            initialized = true;

            roomIdGetter.Ready += OnRoomIdGetterReady;

            roomIdGetter.Initialize();
        }

        public void Finish()
        {
            if (!initialized) { return; }

            initialized = false;
            roomIdGetter.Ready -= OnRoomIdGetterReady;

            roomIdGetter.Finish();
        }

        public ServerRoomData<TMatchConfig, TPlayerInfo> GetRoomData<TMatchConfig, TPlayerInfo>()
        {
            return new ServerRoomData<TMatchConfig, TPlayerInfo>(serializer, roomDataPayload);
        }
        
        void OnRoomIdGetterReady()
        {
            GetRoomData();
        }

        void GetRoomData()
        {
            roomRequestor.GetRoomData(roomIdGetter.RoomId,
                onSuccess: response =>
                {
                    roomDataPayload = response;
                    
                    NotifyReady();
                },
                onError: error =>
                {
                    Debug.Log($"GetRoomData | Error | [code: {error.Code}] [message: {error.Message}]");
                });
        }

        void NotifyReady()
        {
            roomRequestor.SetRoomReady(roomIdGetter.RoomId,
                onSuccess: () =>
                {
                    Ready?.Invoke();
                },
                onError: error =>
                {
                    Debug.Log($"SetRoomReady | Error | [code: {error.Code}] [message: {error.Message}]");
                });
        }
    }
}