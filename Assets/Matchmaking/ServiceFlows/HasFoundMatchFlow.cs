using System;
using System.Collections;
using Matchmaking.Constants;
using Matchmaking.Requestors;
using Matchmaking.Models;
using MetagameRequestor;
using MetagameRequestor.Models;
using UnityEngine;

namespace Matchmaking.ServiceFlows
{
    public class HasFoundMatchFlow
    {
        private readonly MatchmakingConfig config;
        private readonly ICoroutineHelper coroutineHelper;
        private readonly IMatchmakingRequestor matchmakingRequestor;

        private Coroutine coroutineRun;

        private bool IsError { set; get; }
        private bool IsSuccess { set; get; }

        private Error Error { set; get; }
        private HasFoundMatchResponse Response { set; get; }

        public HasFoundMatchFlow(MatchmakingConfig config, 
            ICoroutineHelper coroutineHelper,
            IMatchmakingRequestor matchmakingRequestor)
        {
            this.config = config;
            this.coroutineHelper = coroutineHelper;
            this.matchmakingRequestor = matchmakingRequestor;

            coroutineRun = null;
        }

        public void Run(string userId, Action<HasFoundMatchResponse> onSuccess, Action<Error> onError)
        {
            Stop();

            coroutineHelper.StartCoroutine(CoroutineRun(userId, onSuccess, onError));
        }

        public void Stop()
        {
            if (coroutineRun != null)
            {
                coroutineHelper.StopCoroutine(coroutineRun);
            }
        }

        private IEnumerator CoroutineRun(string userId, Action<HasFoundMatchResponse> onSuccess, Action<Error> onError)
        {
            var isTimeout = false;
            var startTime = DateTime.UtcNow;

            IsError = false;
            IsSuccess = false;

            do
            {
                yield return HasFoundMatch(userId);
                
                if (IsError || (IsSuccess && Response.hasFoundMatch)) { break; }
                
                yield return new WaitForSeconds(1.0f);

                var deltaTime = startTime - DateTime.UtcNow;
                isTimeout = deltaTime.Seconds >= config.joinMatchTimeoutSeconds;

            } while (!isTimeout);

            if (isTimeout)
            {
                IsError = true;
                IsSuccess = false;

                Error = new Error(Errors.TimeoutErrorCode, Errors.TimeoutErrorMessage);
            }

            if (IsSuccess)
            {
                onSuccess?.Invoke(Response);
            }
            else
            {
                onError?.Invoke(Error);
            }
        }
        
        private IEnumerator HasFoundMatch(string userId)
        {
            var requestFinished = false;

            matchmakingRequestor.HasFoundMatch(userId,
                onSuccess: response =>
                {
                    Debug.Log($"HasFoundMatch | Success | [hasFoundMatch: {response.hasFoundMatch}] [roomId: {response.roomId}]");
                    
                    IsError = false;
                    IsSuccess = true;
                    
                    Response = response;

                    requestFinished = true;
                },
                onError: error =>
                {
                    Debug.Log($"HasFoundMatch | Error | [code: {error.Code}] [message: {error.Message}]");

                    IsError = true;
                    IsSuccess = false;
                    
                    Error = error;

                    requestFinished = true;
                });

            while (!requestFinished) { yield return null; }
        }
    }
}