using System;

namespace Matchmaking.Models
{
    [Serializable]
    public class HasFoundMatchResponse
    {
        public string roomId;
        public bool hasFoundMatch;
    }
}