using System;

namespace Matchmaking.Models
{
    [Serializable]
    public class IsRoomReadyResponse
    {
        public bool isRoomReady;
        public ClientRoomData roomData;
    }
}