using System;
using System.Linq;
using MetagameRequestor.Serializer;

namespace Matchmaking.Models
{
    [Serializable]
    public class ServerRoomDataPayload
    {
        public string serverHost;
        public int serverPort;
        public string gameMode;
        public int numberOfPlayers;
        public object matchConfig;
        public object[] playerInfos;
    }
    
    [Serializable]
    public class ServerRoomData<TMatchConfig, TPlayerInfo>
    {
        private readonly ServerRoomDataPayload payload;

        private TMatchConfig matchConfig;
        private TPlayerInfo[] playerInfos;

        public string ServerHost => payload.serverHost;
        public int ServerPort => payload.serverPort;
        public string GameMode => payload.gameMode;
        public int NumberOfPlayers => payload.numberOfPlayers;
        public TMatchConfig MatchConfig => matchConfig;
        public TPlayerInfo[] PlayerInfos => playerInfos;
        
        public ServerRoomData(ISerializer serializer, ServerRoomDataPayload payload)
        {
            this.payload = payload;
            
            matchConfig = serializer.FromJson<TMatchConfig>(payload.matchConfig.ToString());
            playerInfos = payload.playerInfos.Select(p => p.ToString()).Select(serializer.FromJson<TPlayerInfo>).ToArray();
        }
    }
}