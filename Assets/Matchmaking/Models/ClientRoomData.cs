using System;

namespace Matchmaking.Models
{
    [Serializable]
    public class ClientRoomData
    {
        public string serverHost;
        public int serverPort;
        public string gameMode;
    }
}