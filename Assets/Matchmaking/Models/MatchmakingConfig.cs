using System;

namespace Matchmaking.Models
{
    [Serializable]
    public class MatchmakingConfig
    {
        public float joinMatchTimeoutSeconds;
        public float waitRoomReadyTimeoutSeconds;
    }
}