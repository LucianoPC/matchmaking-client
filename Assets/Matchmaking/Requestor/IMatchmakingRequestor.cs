using System;
using Matchmaking.Models;
using MetagameRequestor.Models;

namespace Matchmaking.Requestors
{
    public interface IMatchmakingRequestor
    {
        void JoinMatch(string userId, Action onSuccess, Action<Error> onError);
        void StopJoinMatch(string userId, Action onSuccess, Action<Error> onError);
        void HasFoundMatch(string userId, Action<HasFoundMatchResponse> onSuccess, Action<Error> onError);
        void IsRoomReady(string roomId, Action<IsRoomReadyResponse> onSuccess, Action<Error> onError);
    }
}