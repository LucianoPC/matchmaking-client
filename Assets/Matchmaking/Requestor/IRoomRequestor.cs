using System;
using Matchmaking.Models;
using MetagameRequestor.Models;

namespace Matchmaking.Requestors
{
    public interface IRoomRequestor
    {
        void SetRoomReady(string roomId, Action onSuccess, Action<Error> onError);
        void GetRoomData(string roomId, Action<ServerRoomDataPayload> onSuccess, Action<Error> onError);
    }
}