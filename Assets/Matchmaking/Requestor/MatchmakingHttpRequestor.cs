using System;
using Matchmaking.Models;
using MetagameRequestor;
using MetagameRequestor.Models;

namespace Matchmaking.Requestors
{
    public class MatchmakingHttpRequestor : IMatchmakingRequestor
    {
        private readonly HttpRequestor httpRequestor;

        public MatchmakingHttpRequestor(HttpRequestor httpRequestor)
        {
            this.httpRequestor = httpRequestor;
        }

        public void JoinMatch(string userId, Action onSuccess, Action<Error> onError)
        {
            var parameters = new 
            {
                userId = userId,
            };

            httpRequestor.Post("/plugin/matchmaking/joinMatch", parameters, onSuccess, onError);
        }
        
        public void StopJoinMatch(string userId, Action onSuccess, Action<Error> onError)
        {
            var parameters = new 
            {
                userId = userId,
            };

            httpRequestor.Post("/plugin/matchmaking/stopJoinMatch", parameters, onSuccess, onError);
        }
        
        public void HasFoundMatch(string userId, Action<HasFoundMatchResponse> onSuccess, Action<Error> onError)
        {
            var parameters = new 
            {
                userId = userId,
            };

            httpRequestor.Get<HasFoundMatchResponse>("/plugin/matchmaking/hasFoundMatch", parameters, onSuccess, onError);
        }
        
        public void IsRoomReady(string roomId, Action<IsRoomReadyResponse> onSuccess, Action<Error> onError)
        {
            var parameters = new 
            {
                roomId = roomId,
            };

            httpRequestor.Get<IsRoomReadyResponse>("/plugin/matchmaking/isRoomReady", parameters, onSuccess, onError);
        }
    }
}