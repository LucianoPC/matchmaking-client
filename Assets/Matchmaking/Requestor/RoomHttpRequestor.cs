using System;
using Matchmaking.Models;
using MetagameRequestor;
using MetagameRequestor.Models;

namespace Matchmaking.Requestors
{
    public class RoomHttpRequestor : IRoomRequestor
    {
        private readonly HttpRequestor httpRequestor;

        public RoomHttpRequestor(HttpRequestor httpRequestor)
        {
            this.httpRequestor = httpRequestor;
        }

        public void SetRoomReady(string roomId, Action onSuccess, Action<Error> onError)
        {
            var parameters = new 
            {
                roomId = roomId,
            };

            httpRequestor.Post("/plugin/matchmaking/room/setRoomReady", parameters, onSuccess, onError);
        }

        public void GetRoomData(string roomId, Action<ServerRoomDataPayload> onSuccess, Action<Error> onError)
        {
            var parameters = new
            {
                roomId = roomId,
            };

            httpRequestor.Get("/plugin/matchmaking/room/getRoomData", parameters, onSuccess, onError);
        }
    }
}