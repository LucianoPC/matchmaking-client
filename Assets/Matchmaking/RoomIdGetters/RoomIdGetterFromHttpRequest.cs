using System;
using System.Net;
using System.Threading;
using MetagameRequestor;
using UnityEngine;

namespace Matchmaking.RoomIdGetters
{
    public class RoomIdGetterFromHttpRequest : IRoomIdGetter
    {
        // Attributes
        readonly int listenPort;
        
        // Server
        readonly HttpListener server;
        
        // Control
        Thread thread;

        // Properties
        public string RoomId { get; private set; }
        
        // Events
        public event Action Ready;

        public RoomIdGetterFromHttpRequest(int listenPort)
        {
            this.listenPort = listenPort;
            
            server = new HttpListener();
            server.Prefixes.Add ($"http://*:{listenPort}/");
            server.AuthenticationSchemes = AuthenticationSchemes.Anonymous;
        }

        public void Initialize()
        {
            StartServerAndWaitRequestForStart();
        }

        public void Finish()
        {
            thread?.Interrupt();
            server.Stop();
        }
        
        void StartServerAndWaitRequestForStart()
        {
            Debug.LogFormat("MatchRoomService Waiting RoomID :: [isSupported: {0}]", HttpListener.IsSupported);
            
            server.Start();
            
            thread = new Thread(() =>
            {
                var context = server.GetContext();
                var request = context.Request;
                var response = context.Response;
                
                var responseString = "Ok!";
                var buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
                
                response.ContentLength64 = buffer.Length;
                var outputStream = response.OutputStream;
                outputStream.Write(buffer,0,buffer.Length);
                outputStream.Close();
                
                MainQueueDispatcher.Dispatch(() =>
                {
                    server.Stop();

                    RoomId = request.QueryString.Get("roomId");
                    
                    Debug.LogFormat("MatchRoomService Received RoomID :: [request: {0}]", string.Join(" :: ", request.QueryString.Get("roomId")));
                    
                    Ready?.Invoke();
                });
            });
            
            thread.Start();
        }
    }
}