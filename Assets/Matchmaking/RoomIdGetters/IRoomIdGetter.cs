using System;

namespace Matchmaking.RoomIdGetters
{
    public interface IRoomIdGetter
    {
        string RoomId { get; }

        event Action Ready; 

        void Initialize();
        void Finish();
    }
}