using System;

namespace Matchmaking.RoomIdGetters
{
    public class RoomIdGetterFromEnvironmentVar : IRoomIdGetter
    {
        public string RoomId { get; private set; }
        
        public event Action Ready;
        
        public void Initialize()
        {
            RoomId = Environment.GetEnvironmentVariable("ROOM_ID");
            
            Ready?.Invoke();
        }

        public void Finish()
        {
        }
    }
}