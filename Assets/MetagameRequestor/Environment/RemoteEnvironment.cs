namespace MetagameRequestor.Environment
{
    internal class RemoteEnvironment : IEnvironment
    {
        public string HttpServerURL => "http://remote.environment.com";
    }
}