namespace MetagameRequestor.Environment
{
    public interface IEnvironment
    {
        string HttpServerURL { get; }
    }
}