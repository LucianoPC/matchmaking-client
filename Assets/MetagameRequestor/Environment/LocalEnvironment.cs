namespace MetagameRequestor.Environment
{
    internal class LocalEnvironment : IEnvironment
    {
        public string HttpServerURL => "http://localhost";
    }
}