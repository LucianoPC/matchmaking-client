using UnityEngine;

namespace MetagameRequestor.Environment
{
    public static class EnvironmentChooser
    {
        public enum EnvironmentMode
        {
            Local = 0,
            Remote = 1,
        }

        private const int DefaultEnvironmentMode = 0;
        private const string CurrentEnvironmentKey = "EnvironmentChooser::CurrentEnvironmentKey";
        
        private static readonly LocalEnvironment localEnvironment = new LocalEnvironment();
        private static readonly RemoteEnvironment remoteEnvironment = new RemoteEnvironment();

        private static bool hasLoadedCurrentEnvironment;
        private static IEnvironment currentEnvironment;
        private static EnvironmentMode currentEnvironmentMode;

        public static IEnvironment CurrentEnvironment
        {
            get
            {
                LoadCurrentEnvironmentIfItHasNotLoaded();
                return currentEnvironment;
            }
        }

        public static EnvironmentMode CurrentEnvironmentMode
        {
            get
            {
                LoadCurrentEnvironmentIfItHasNotLoaded();
                return currentEnvironmentMode;
            }
        }

        public static string HttpServerURL => CurrentEnvironment.HttpServerURL;

        public static bool IsLocalEnvironment => CurrentEnvironment == localEnvironment;
        public static bool IsRemoteEnvironment => CurrentEnvironment == remoteEnvironment;

        public static void SetCurrentEnvironment(EnvironmentMode environmentMode)
        {
            currentEnvironmentMode = environmentMode;
            currentEnvironment = environmentMode switch
            {
                EnvironmentMode.Local => localEnvironment,
                EnvironmentMode.Remote => remoteEnvironment,
                _ => remoteEnvironment
            };

            var environmentNumber = (int) environmentMode;
            PlayerPrefs.SetInt(CurrentEnvironmentKey, environmentNumber);
            PlayerPrefs.Save();
        }

        private static void LoadCurrentEnvironmentIfItHasNotLoaded()
        {
            if (hasLoadedCurrentEnvironment) { return; }
            hasLoadedCurrentEnvironment = true;
            
            LoadCurrentEnvironment();
        }

        private static void LoadCurrentEnvironment()
        {
            var environmentMode = (EnvironmentMode) PlayerPrefs.GetInt(CurrentEnvironmentKey, DefaultEnvironmentMode);
            
            SetCurrentEnvironment(environmentMode);
        }
    }
}