using MetagameRequestor.Environment;
using MetagameRequestor.Serializer;

namespace MetagameRequestor
{
    public static class RequestorFactory
    {
        public static HttpRequestor NewHttpRequestor(ISerializer serializer, ICoroutineHelper coroutineHelper)
        {
            var serverURL = EnvironmentChooser.HttpServerURL;

            return new HttpRequestor(serverURL, serializer, coroutineHelper);
        }
        
        public static HttpRequestor NewHttpRequestor(ICoroutineHelper coroutineHelper)
        {
            var serializer = new SimpleJsonSerializer();

            return NewHttpRequestor(serializer, coroutineHelper);
        }
    }
}