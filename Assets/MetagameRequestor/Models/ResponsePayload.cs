namespace MetagameRequestor.Models
{
    public class ResponsePayload
    {
        public string code;
        public string message;

        public object data;
    }
}