using System.Collections;
using UnityEngine;

namespace MetagameRequestor
{
    public interface ICoroutineHelper
    {
        Coroutine StartCoroutine(IEnumerator routine);
        void StopCoroutine(Coroutine routine);
    }
}