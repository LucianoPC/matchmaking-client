using System;
using System.Collections;
using System.Text;
using MetagameRequestor.Models;
using MetagameRequestor.Serializer;
using UnityEngine;
using UnityEngine.Networking;

namespace MetagameRequestor
{
    public class HttpRequestor
    {
        const string SuccessCode = "RP-200";

        const string NoInternetErrorCode = "Error-001";
        const string HttpErrorCode = "Error-002";

        private readonly string serverURL;
        private readonly ISerializer serializer;
        private readonly ICoroutineHelper coroutineHelper;

        public event Action<Error> Error;

        public HttpRequestor(string serverURL, ISerializer serializer, ICoroutineHelper coroutineHelper)
        {
            this.serverURL = serverURL;
            this.serializer = serializer;
            this.coroutineHelper = coroutineHelper;
        }
        
        public void Post(string route, object request, Action onSuccess = null, Action<Error> onError = null)
        {
            Post<string>(route, request,
                onSuccess: _ =>
                {
                    onSuccess?.Invoke();
                },
                onError: onError);
        }

        public void Post<T>(string route, object parameters, Action<T> onSuccess = null, Action<Error> onError = null)
        {
            coroutineHelper.StartCoroutine(CoroutineRequest(route, parameters, "POST", onSuccess, onError));
        }
        
        public void Get(string route, object request, Action onSuccess = null, Action<Error> onError = null)
        {
            Get<string>(route, request,
                onSuccess: _ =>
                {
                    onSuccess?.Invoke();
                },
                onError: onError);
        }

        public void Get<T>(string route, object parameters, Action<T> onSuccess, Action<Error> onError)
        {
            coroutineHelper.StartCoroutine(CoroutineRequest(route, parameters, "GET", onSuccess, onError));
        }
        
        private IEnumerator CoroutineRequest<T>(string route, object parameters, string method, Action<T> onSuccess, Action<Error> onError)
        {
            var url = $"{serverURL}{route}";
            var json = serializer.ToJson(parameters);

            var www = new UnityWebRequest(url, method);
            byte[] bodyRaw = Encoding.UTF8.GetBytes(json);
            www.timeout = 10;
            www.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            www.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            www.SetRequestHeader("Content-Type", "application/json");

            Debug.Log($"[HTTP] {method} [route: {route}] [data: {json}]");

            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                var error = new Error(NoInternetErrorCode, www.error);
                Debug.Log($"[HTTP] {method} error [route: {route}] [code: {error.Code}] [message: {error.Message}]");
                onError?.Invoke(error);
            }
            else if (www.isHttpError)
            {
                var error = new Error(HttpErrorCode, www.error);
                Debug.Log($"[HTTP] {method} error [route: {route}] [code: {error.Code}] [message: {error.Message}]");
                onError?.Invoke(error);
            }
            else
            {
                var responsePayload = SimpleJson.SimpleJson.DeserializeObject<ResponsePayload>(www.downloadHandler.text);
                var isSuccess = responsePayload.code == SuccessCode;

                if (isSuccess)
                {
                    var dataText = responsePayload.data == null ? string.Empty : responsePayload.data.ToString();
                    var data = serializer.FromJson<T>(dataText);

                    Debug.Log($"[HTTP] {method} SUCCESS [route: {route}] [data: {data}]");

                    onSuccess?.Invoke(data);
                }
                else
                {
                    var error = new Error(responsePayload.code, responsePayload.message);

                    Debug.Log($"[HTTP] {method} ERROR [route: {route}] [code: {error.Code}] [message: {error.Message}]");

                    onError?.Invoke(error);
                }
            }
        }
    }
}